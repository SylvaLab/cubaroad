# -*- coding: utf-8 -*-
"""
Software: CubaRoad
File: CubaRoad_0_param.py
Copyright (C) Sylvain DUPIRE - SylvaLab 2021
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@sylvalab.fr
Version: 2.2
Date: 2021/06/09
License :  GNU-GPL V3
"""

from CubaRoad_1_function import *

###############################################################################
### Parameters
###############################################################################

Dtm_file = "F:/SylvaLab/Projets/ONF/ONF_Desserte/Work/test_data/Zone test/Col_du_pre/Input/mnt.tif"
Road_file =  'F:/SylvaLab/Projets/ONF/ONF_Desserte/Work/test_data/Zone test/Col_du_pre/Res/Simu_51_Pl(2-10)_Pt(90-70)_Pen(60-80)_D(40)/New_Troncon_1.shp'
Res_dir =  "F:/SylvaLab/Projets/ONF/ONF_Desserte/Work/test_data/Zone test/Col_du_pre/Res/"

from_Sylvaroad = False  # True if Road_file from SylvaRoad / False if Road_file is not from SylvaRoad
step = 10               # [m] Step of analysis
max_exca_slope = 60     # [%] Cross slope beyond which 100% of material is excavated
min_exca_slope = 25     # [%] Inferior threshold of cross slope to start skidding the road axis
z_tolerance = 5         # [cm] tolerance to consider an intercetion with terrain 
xy_tolerance = 2        # [m] buffer around theoretical axis of the road 
                        # Put None to have xy_tolerance = 0.5*road_width
save_fig = False        # Save (True) or not (False) the ground profile of each calculation point  
save_shp = True         # Save (True) or not (False) the point of analyse and transects

Radius = 8              # [m] Radius of lace turns
angle_hairpin = 110     # [°] Min angle to be considered as lace turn

################################################################################
### Script execution
################################################################################
if 'Wspace' not in locals() or 'Wspace' not in globals() :
    Wspace = Res_dir


#Run road designer 
apply_cubaroad(Dtm_file,Road_file,Res_dir,step,max_exca_slope,
               min_exca_slope,z_tolerance,xy_tolerance,save_fig,
               save_shp,Wspace,from_Sylvaroad,Radius,angle_hairpin)


